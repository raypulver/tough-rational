# tough-rational

A library for handling arbitrary-precision rational values, backed by BigInt if it is supported in the envrionment, otherwise will fall back to bignumber.js

This library is designed to be used as a drop in replacement for existing big number libraries, thus exports many method aliases for convenience.

## docs

Create a Rational with the constructor, (new is optional)

All methods support 0 or 1 argument. Arguments are always coerced to a tough-rational
Supported methods on the Rational prototype include (with aliases in parentheses):

* plus (add)
* minus (subtract, sub)
* dividedBy (div, divide)
* dividedToIntegerBy (idiv, divToInt)
* multiply (mul, multipliedBy)
* toString (toFormat, toDecimal, toPrecision) (this method accepts a decimal precision argument to render to strings of desired precision)
* div10
* pow10
* pow
* leq (lte)
* geq (gte)
* gt
* lt
* eq (equals)
* neq
* abs
* ceil
* floor
* fractionalComponent
* gtZero
* isFinite
* isNaN
* isNonZero
* isZero
* ltZero
* mod
