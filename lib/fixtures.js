'use strict';

const util = require('./util');
const MARKER = Symbol('rational_marker');
const isEqualToMarker = util.equals(MARKER);

const isRational = util.flow(util.property('_marker'), isEqualToMarker);
const decorateRationalMarker = (o) => Object.assign(o, {
  _marker: MARKER
});

Object.assign(module.exports, {
  MARKER,
  isRational,
  decorateRationalMarker
});
