'use strict';

const util = require('./util');

module.exports = (proto, aliases) => Object.assign(proto, aliases.reduce((r, [ src, dest ]) => Object.assign(r, util.castArray(dest).reduce((mixin, v) => {
  mixin[v] = function (...args) { return this[src].apply(this, args); };
  return mixin;
}, {})), {}));
