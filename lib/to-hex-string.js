'use strict';

const util = require('./util');

const toBytes = (Rational) => (rational) => {
  rational = Rational(rational).abs();
  let remaining = rational.floor();
  let byte = 0;
  const retval = [];
  while (remaining.isNonZero()) {
    const mod = remaining.mod(Rational(1).shiftLeft(++byte * 8)).and(0xff);
    remaining = remaining.shiftRight(8);
    retval.unshift(parseInt(String(mod), 10));
  }
  return retval;
};

const zeroPadLeftTo2 = util.leftPad('0')(2);

const toHexString = (Rational) => {
  const _toBytes = toBytes(Rational);
  const retval = Object.assign((s) => {
    let prefixWithMinus = false;
    if (Rational(s).lt(0)) prefixWithMinus = true;
    const result = retval._toBytes(s).reduce((r, v) => r + zeroPadLeftTo2(v.toString(16)), '0x');
    return prefixWithMinus ? '-' + result : result;
  }, {
    _toBytes
  });
  return retval;
};

const leftZeroRe = /(^0+)/;

const toHexStringStripped = (toHexString) => (...args) => {
  const hexStr = toHexString(...args);
  const stripped = hexStr.substr(2);
  return '0x' + stripped.replace(leftZeroRe, '');
};

Object.assign(toHexString, {
  toHexStringStripped
});

module.exports = toHexString;
