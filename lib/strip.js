'use strict';

const util = require('./util');

const stripZeroes = (s) => {
  const decimalPointPosition = s.lastIndexOf('.');
  if (!~decimalPointPosition) return s;
  const stripped = s.substr(0, decimalPointPosition) + '.' + s.substr(decimalPointPosition + 1).replace(/0+$/, '');
  if (util.last(stripped) === '.') return stripped.substr(0, stripped.length - 1);
  return stripped;
};

Object.assign(module.exports, {
  stripZeroes
});
