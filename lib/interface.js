'use strict';

const util = require('./util');
const fixtures = require('./fixtures');
const aliasMixin = require('./alias-mixin');

const rationalInterface = {
  abs: null,
  ceil: null,
  div10: null,
  dividedBy: null,
  dividedToIntegerBy: null,
  eq: null,
  floor: null,
  fractionalComponent: null,
  geq: null,
  gtZero: null,
  gt: null,
  isFinite: null,
  isFloatingPoint: null,
  isInfinity: null,
  isNaN: null,
  isNonZero: null,
  isZero: null,
  leq: null,
  ltZero: null,
  lt: null,
  minus: null,
  mod: null,
  multiply: null,
  neq: null,
  plus: null,
  pow: null,
  pow10: null,
  toString: null,
  _getPrimitives: null,
  _getDenominator: null,
  _getNumerator: null,
  _getEncapsulatedRational: null,
  _setEncapsulatedRational: null
};

const mapToAbstractFunctions = util.mapOwn((v, k) => () => {
  throw new ReferenceError('Function ' + String(k) + ' does not exist in class prototype');
});

module.exports = Object.assign(aliasMixin(fixtures.decorateRationalMarker(mapToAbstractFunctions(rationalInterface)), [
  [
    'dividedBy', [
      'div',
      'divide'
    ]
  ], [
    'multiply', [
      'mul',
      'multipliedBy'
    ],
  ], [
    'dividedToIntegerBy', [
      'idiv',
      'divToInt'
    ]
  ], [
    'leq',
    'lte'
  ], [
    'geq',
    'gte'
  ], [
    'eq',
    'equals'
  ], [
    'toString', [
      'toFormat',
      'toPrecision'
     ]
  ], [
    'plus',
    'add'
  ], [
    'minus', [
      'subtract',
      'sub'
    ]
  ]
]));
