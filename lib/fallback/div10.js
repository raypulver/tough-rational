'use strict';

const BN = require('bignumber.js');

module.exports = (a, b) => a.div(new BN(10).pow(b));
