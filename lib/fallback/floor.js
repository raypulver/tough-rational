'use strict';

const util = require('../util');

module.exports = util.method('dividedToIntegerBy', 1);
