'use strict';

module.exports = () => new Function('a', 'b', 'return a ** b;');
