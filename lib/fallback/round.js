'use strict';

const fractionalComponent = require('./fractional-component');

module.exports = (n) => fractionalComponent(n).gte(0.5) ? n.dividedToIntegerBy(1).plus(1) : n.dividedToIntegerBy(1);
