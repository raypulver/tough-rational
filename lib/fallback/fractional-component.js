'use strict';

module.exports = (n) => n.minus(n.dividedToIntegerBy(1));
