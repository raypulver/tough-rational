'use strict';

const util = require('../util');
const BN = require('bignumber.js');
module.exports = (bn, n) => util.truncateDecimals(bn.toPrecision(), Number(new BN(n || 0)));
