'use strict';

const BN = require('bignumber.js');
module.exports = (a, b) => a.multipliedBy(new BN(10).pow(b));
