'use strict';

module.exports = (n) => !n.isFinite();
