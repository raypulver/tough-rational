'use strict';

module.exports = (n) => !n.isInteger();
