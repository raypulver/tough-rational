'use strict';

const util = require('../util');
module.exports = (bn) => bn.toPrecision();
