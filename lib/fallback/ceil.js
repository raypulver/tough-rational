'use strict';

const fractionalComponent = require('./fractional-component');

module.exports = (n) => fractionalComponent(n).eq(0) ? n : n.dividedToIntegerBy(1).plus(1);
