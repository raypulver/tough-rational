'use strict';

const BN = require('bignumber.js');

module.exports = (n, shift) => n.multipliedBy(new BN(2).pow(shift));
