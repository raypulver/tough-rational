'use strict';

module.exports = (a, b) => !a.eq(b);
