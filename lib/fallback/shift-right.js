'use strict';

const BN = require('bignumber.js');

module.exports = (n, shift) => n.dividedToIntegerBy(new BN(2).pow(shift));
