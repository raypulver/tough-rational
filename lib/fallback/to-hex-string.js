'use strict';

module.exports = (n) => {
  const retval = '0x' + n.toString(16);
  if (retval.length % 2) return '0x0' + retval.substr(2);
  return retval;
};
