'use strict';

const emptyObject = {};

module.exports = (v) => typeof v === 'object' && typeof v.writeUint8 === 'function' && typeof (v.constructor || emptyObject).from === 'function';
