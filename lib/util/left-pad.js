'use strict';

const leftPad = (c) => (n) => (s) => Array(Math.max(1, n - s.length + 1)).join(c) + s;

module.exports = leftPad;
