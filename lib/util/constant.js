'use strict';

const constant = (data) => () => data;

module.exports = constant;
