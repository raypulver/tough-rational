'use strict';

const forOwn = require('./for-own');

const mapOwn = (fn) => (o) => {
  const retval = {};
  forOwn(o, (v, k) => {
    retval[k] = fn(v, k);
  });
  return retval;
};

module.exports = mapOwn;
