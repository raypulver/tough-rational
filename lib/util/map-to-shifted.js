'use strict';

const mapOwn = require('./map-own');
const shiftThis = require('./shift-this');

module.exports = mapOwn(shiftThis);
