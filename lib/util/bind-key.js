'use strict';

const noop = require('./noop');
const bindKey = (o, fn) => function (...args) { return (o[fn] || noop).apply(o, args); };

module.exports = bindKey;
