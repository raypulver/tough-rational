'use strict';

const map = (fn) => (ary) => ary.map(fn);

module.exports = map;
