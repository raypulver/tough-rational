'use strict';

const bindKey = require('./bind-key');
const isArray = bindKey(Array, 'isArray');

const castArray = (data) => !isArray(data) ? data === undefined ? [] : [ data ] : data;

module.exports = castArray;
