'use strict';

const forOwn = (o, fn, thisArg) => Object.keys(o).forEach((key) => fn.call(thisArg, o[key], key, o));

module.exports = forOwn;
