'use strict';

const setter = (prop) => function (v) { this[prop] = v; };
module.exports = setter;
