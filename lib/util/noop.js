'use strict';

const noop = () => {};

module.exports = noop;
