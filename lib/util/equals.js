'use strict';

const equals = (data) => (v) => data === v;

module.exports = equals;
