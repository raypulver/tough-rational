'use strict';

const get = (prop) => (o) => o[prop];
module.exports = get;
