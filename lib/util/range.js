'use strict';

const range = (n) => Array.apply(null, { length: n }).map((v, i) => i);

module.exports = range;
