'use strict';

const { reduce } = [];
const leftPad = require('./left-pad')
const padZero = leftPad('0');
const padZeroEven = padZero(2);

module.exports = (buf) => typeof buf === 'string' ? buf : reduce.call(buf, (r, v) => r + padZeroEven(Number(v).toString(16)), '0x');
