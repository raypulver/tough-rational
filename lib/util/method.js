'use strict';

const method = (fn, ...args) => (o) => o[fn](...args);

module.exports = method;
