'use strict';

const partial = (fn, ...partials) => function (...args) { return fn.apply(this, [ ...partials, ...args ]); };

module.exports = partial;
