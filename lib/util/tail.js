'use strict';

const { slice } = [];

const tail = (ary) => slice.call(ary, 1);

module.exports = tail;
