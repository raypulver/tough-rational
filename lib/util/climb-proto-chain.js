'use strict';

const range = require('./range');

const climbProtoChain = (o, n) => range(n).reduce(() => (o = Object.getPrototypeOf(o)), o);

module.exports = climbProtoChain;
