'use strict';

const partialRight = (fn, ...partials) => function (...args) { return fn.apply(this, [ ...args, ...partials ]); };

module.exports = partialRight;
