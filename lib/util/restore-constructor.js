'use strict';

const restoreConstructor = (classFunction) => Object.defineProperty(classFunction.prototype, 'constructor', {
  enumerable: false,
  value: classFunction
});

module.exports = restoreConstructor;
