'use strict';

const first = require('./first');
const tail = require('./tail');
const noop = require('./noop');

const flow = (...fns) => function (...args) {
  return tail(fns).reduce((retval, fn) => fn.call(this, retval), (first(fns) || noop).apply(this, args));
};

module.exports = flow;
