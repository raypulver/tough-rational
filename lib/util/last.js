'use strict';

const last = (ary) => ary[ary.length - 1];

module.exports = last;
