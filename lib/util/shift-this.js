'use strict';

const shiftThis = (fn) => function (...args) { return fn(...[ this, ...args ]); };

module.exports = shiftThis;
