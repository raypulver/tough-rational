'use strict';

function testForBigInt() {
  try {
    return Boolean(BigInt(1));
  } catch (e) {
    return false;
  }
}

Object.assign(module.exports, {
  testForBigInt
});
