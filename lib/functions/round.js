'use strict';

const fractionalComponent = require('./fractional-component');
const geq = require('./geq');
const floor = require('./floor');
const plus = require('./plus');
const generate = require('./generate');
const partialRight = require('../util/partial-right');
const addOne = partialRight(plus, generate.generateOne());
const compat = require('../compat');
const oneHalf = compat.testForBigInt() && [ BigInt(1), BigInt(2) ];
const geqOneHalf = partialRight(geq, oneHalf);

const round = (n) => geqOneHalf(fractionalComponent(n)) ? addOne(floor(n)) : floor(n);

module.exports = round;
