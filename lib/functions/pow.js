'use strict';

const powGenerator = require('./pow-generator');
const pow = powGenerator();

module.exports = ([ aNum, aDen ], [ bNum, bDen ]) => {
  const bInt = bNum / bDen;
  return [ pow(aNum, bInt), pow(aDen, bInt) ];
};
