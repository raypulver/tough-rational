'use strict';

const dividedToIntegerBy = require('./divided-to-integer-by');
const pow = require('./pow');

module.exports = (a, [ num, den ]) => dividedToIntegerBy(a, pow([ BigInt(2), BigInt(1) ], [ num / den, BigInt(1) ]));
