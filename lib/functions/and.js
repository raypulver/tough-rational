'use strict';

module.exports = ([ aNum, aDen ], [ bNum, bDen ]) => [ (aNum / aDen) & (bNum / bDen), BigInt(1) ];
