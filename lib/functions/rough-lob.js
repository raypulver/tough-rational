'use strict';

const compat = require('../compat');
    
let BASE;
let LOBMASK_I, LOBMASK_BI;

if (compat.testForBigInt()) {
  BASE = BigInt(1e7);
  LOBMASK_I = BigInt(1) << BigInt(30), LOBMASK_BI = (BASE & -BASE) * (BASE & -BASE) | LOBMASK_I;
}

module.exports = (n) => {
  const v = n, x = typeof v === "bigint" ? v | LOBMASK_I : v * BASE | LOBMASK_BI;
  return x & -x;
};
