'use strict';

const eq = require('./eq');
const gt = require('./gt');

module.exports = (a, b) => eq(a, b) ? 0 : gt(a, b) ? 1 : -1;
