'use stricf';

const util = require('../util');
const generate = require('./generate');
const gt = require('./gt');

module.exports = util.partialRight(gt, generate.generateZero());
