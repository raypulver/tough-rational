'use strict';

const gcd = require('./gcd');

module.exports = ([ aNum, aDen ], [ bNum, bDen ]) => [ aNum * bNum, aDen * bDen ];
