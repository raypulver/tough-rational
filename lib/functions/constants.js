'use strict';

const NAN = Symbol('NaN');
const INFINITY = Symbol('Infinity');
const NEGATIVE_INFINITY = Symbol('-Infinity');

Object.assign(module.exports, {
  NAN,
  INFINITY,
  NEGATIVE_INFINITY
});
