'use strict';

const util = require('../util');
const { NAN } = require('./constants');
const equalsNaN = util.equals(NAN);

module.exports = util.flow(util.first, equalsNaN);
