'use strict';

const gcd = require('./gcd');

module.exports = ([ aNum, aDen ], [ bNum, bDen ]) => {
  if (!bNum) throw Error('Cannot divide by zero');
  return [ aNum * bDen, aDen * bNum ];
};
