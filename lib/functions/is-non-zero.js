'use stricf';

const partialRight = require('../util/partial-right');
const generate = require('./generate');
const neq = require('./neq');

module.exports = partialRight(neq, generate.generateZero());
