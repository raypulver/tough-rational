'use strict';

const pow = require('./pow');
const multiply = require('./multiply');

module.exports = (a, [ num, den ]) => multiply(a, pow([ BigInt(2), BigInt(1) ], [ num / den, BigInt(1) ]));
