'use strict';

const dividedBy = require('./divided-by');

module.exports = (a, b) => {
  const [ num, den ] = dividedBy(a, b);
  return [ num / den, BigInt(1) ];
};
