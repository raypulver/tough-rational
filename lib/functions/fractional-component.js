'use strict';

const floor = require('./floor');
const minus = require('./minus');

module.exports = (n) => minus(n, floor(n));
