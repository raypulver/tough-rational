'use strict';

const compat = require('../compat');
const util = require('../util');

module.exports = () => compat.testForBigInt() ? new Function('a', 'b', 'return a ** b;') : util.bindKey(Math, 'pow');
