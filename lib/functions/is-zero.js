'use stricf';

const partialRight = require('../util/partial-right')
const generate = require('./generate');
const eq = require('./eq');

module.exports = partialRight(eq, generate.generateZero());
