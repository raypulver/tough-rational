'use strict';

module.exports = ([ aNum, aDen ], [ bNum, bDen ]) => aNum*bDen !== bNum*aDen;
