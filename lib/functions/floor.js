'use strict';

const dividedToIntegerBy = require('./divided-to-integer-by');
const util = require('../util');
const generate = require('./generate');

module.exports = util.partialRight(dividedToIntegerBy, generate.generateOne());
