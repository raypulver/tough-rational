'use strict'

const dividedBy = require('./divided-by');
const fractionalComponent = require('./fractional-component');
const multiply = require('./multiply');

module.exports = (a, b) => multiply(fractionalComponent(dividedBy(a, b)), b);
