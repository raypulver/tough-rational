'use strict';

Object.assign(module.exports, {
  absCoerced: (n) => BigInt(n) < BigInt(0) ? -n : n,
  absNonCoerced: (n) => n < BigInt(0) ? -n : n,
  absPrototypeMethod: ([ num, den ]) => num < 0 ? [ -num, den ] : [ num, den ]
})
