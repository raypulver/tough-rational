'use strict';

const { INFINITY } = require('./constants');
const util = require('../util');
const equalsInfinity = util.equals(INFINITY);

module.exports = util.flow(util.first, equalsInfinity);
