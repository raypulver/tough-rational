'use strict';

const util = require('../util');
const generate = require('./generate');
const lt = require('./lt');

module.exports = util.partialRight(lt, generate.generateZero());
