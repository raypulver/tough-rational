'use strict';

const isInfinity = require('./is-infinity');

module.exports = (n) => !isInfinity(n);
