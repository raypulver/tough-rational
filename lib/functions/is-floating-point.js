'use strict';

const util = require('../util');
const gtZero = require('./gt-zero');
const fractionalComponent = require('./fractional-component');

module.exports = util.flow(fractionalComponent, gtZero);
