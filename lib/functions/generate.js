'use strict';

const parse = require('./parse');
const compat = require('../compat');
const { NAN, INFINITY, NEGATIVE_INFINITY } = require('./constants');

const generateZero = compat.testForBigInt() ? () => [ BigInt(0), BigInt(1) ] : () => [ 0, 1 ];
const generateNaN = () => [ NAN, NAN ];
const generateInfinity = () => [ INFINITY, BigInt(1) ];
const generateNegativeInfinity = () => [ NEGATIVE_INFINITY, BigInt(1) ];
const generateOne = compat.testForBigInt() ? () => [ BigInt(1), BigInt(1) ] : () => [ 1, 1 ];

Object.assign(module.exports, {
  generateZero,
  generateNaN,
  generateInfinity,
  generateNegativeInfinity,
  generateOne
});
