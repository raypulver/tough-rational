'use strict';

const util = require('../util');
const parse = require('./parse');
const compat = require('../compat');
const isFloatingPoint = require('./is-floating-point');
const addOneToFloor = util.flow(require('./floor'), util.partialRight(require('./plus'), compat.testForBigInt() ? parse.parseFromBigInt(BigInt(1)) : [ 1, 1 ]));

module.exports = (n) => isFloatingPoint(n) ? addOneToFloor(n) : floor(n);
