'use strict';

const util = require('./util');
const RationalSupported = require('./rational-supported');
const RationalFallback = require('./rational-fallback');
const compat = require('./compat');
const toHexString = require('./to-hex-string');

const isEqualToRationalFallbackPrototype = util.equals(RationalFallback.prototype);
const isUsingFallback = util.flow(util.constant(Rational), util.property('prototype'), isEqualToRationalFallbackPrototype);

function Rational(input) {
  if (!(this instanceof Rational)) return new Rational(input);
  if (isUsingFallback()) RationalFallback.call(this, input);
  else RationalSupported.call(this, input);
}

const useFallback = (yesOrNo) => {
  if (yesOrNo) {
    Rational.prototype = RationalFallback.prototype;
  } else {
    Rational.prototype = RationalSupported.prototype;
  }
};

useFallback(!compat.testForBigInt());

const toHexStringMethod = toHexString(Rational);
const toHexStringStripped = toHexString.toHexStringStripped(toHexStringMethod);

Object.assign(util.climbProtoChain(Rational.prototype, 2), {
  toHexString() { return toHexStringMethod(this.toString()); },
  toHexStringStripped() { return toHexStringStripped(this.toString()); }
});

module.exports = Object.assign(Rational, {
  useFallback,
  isUsingFallback
});
